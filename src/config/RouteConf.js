const routerConf = (express, app) => {
    app.use('/auth', require('../routes/auth'));
    app.use('/hacker', require('../routes/hacker'));
    app.use('/profile', require('../routes/profile'));
    app.use('/domain', require('../routes/domain'));
    app.use('/image', require('../routes/image'));
};

module.exports = routerConf;

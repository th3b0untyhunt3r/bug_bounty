const express = require('express');
const router = express.Router();
const ErrorResponse = require('../utils/errorResponse');
const { PERMISSIONS } = require('../constants/permissions');
const { protect } = require('../middleware/protect');
const { setHackerAvatar , setCompanyLogo , getHackerAvatar , getCompanyLogo } = require('../controllers/imageController')
const { uploadImageHandler } = require('../middleware/imageHandler');
const { PATH } = require('../constants/paths');


// Avatar upload route for Hackers
router.post('/avatar',
    protect(PERMISSIONS.ONLY_HACKERS), 
    uploadImageHandler(PATH.AVATAR).single('image'),
    setHackerAvatar
);

//Logo upload route for Companies
router.post('/logo', 
    protect(PERMISSIONS.ONLY_COMPANIES),
    uploadImageHandler(PATH.LOGO).single('image'),
    setCompanyLogo
);


// Get Hacker Avatar - Only Authenticated Can See
// Get Company Logo - Everyone
router.get('/avatar/:path', protect(PERMISSIONS.ONLY_AUTHENTICATED), getHackerAvatar);
router.get('/logo/:path', protect(PERMISSIONS.ONLY_AUTHENTICATED), getCompanyLogo);


module.exports = router;
const nodemailer = require('nodemailer');

exports.sendMail = ({ mailTo, mailType, options }) => {
    try {
        const mailOptions = setOptions(mailTo, mailType, options);

        const transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: process.env.EMAIL,
                pass: process.env.PASSWORD,
            },
        });
        transporter.sendMail(mailOptions);
    } catch (err) {
        console.log(err);
        throw new ErrorResponse('Error occured while sending email :(', 500);
    }
};

const setOptions = (mailTo, mailType, options) => {
    const result = {
        from: `"SecUrZone" ${process.env.EMAIL}`,
        to: mailTo,
    };

    switch (mailType) {
        case 'HACKER_REGISTRATION':
            result.subject = `Welcome ${options.username}!`;
            result.html = `
        <h3>You have succesfully created your account.</h3></br>
        <p>Dear hacker,</p>
        <p>Please <a href='${process.env.WEBSITE_URL}/auth/verify/${options.token}'>click</a> to activate your account:</p>
        `;
            break;
        case 'COMPANY_REGISTRATION':
            result.subject = `Welcome ${options.company_name}!`;
            result.html = `
      <h3>You have succesfully created your account.</h3></br>
      <p>Dear <i>${options.fullname}</i>,</p>
      <p>Please <a href='${process.env.WEBSITE_URL}/auth/verify/${options.token}'>click</a> to activate your account:</p>
      `;
            break;
        case 'HACKER_PASSWORD_RESET':
            result.subject = `Reset Password`;
            result.html = `
        <h3>You have succesfully reset your password.</h3></br>
        <p>Dear <i>${options.username}</i>,</p>
        <p>Please <a href='${process.env.WEBSITE_URL}/auth/hacker/reset/${options.token}'>click</a> to activate your account:</p>
      `;
            break;
        case 'RESPONSIBLE_PASSWORD_RESET':
            result.subject = `Reset Password`;
            result.html = `
        <h3>You have succesfully reset your password.</h3></br>
        <p>Dear <i>${options.firstName}</i>,</p>
        <p>Please <a href='${process.env.WEBSITE_URL}/auth/company/reset/${options.token}'>click</a> to activate your account:</p>
      `;
            break;
    }

    return result;
};

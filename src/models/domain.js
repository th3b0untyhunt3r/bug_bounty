const mongoose = require('mongoose');

const domainSchema = mongoose.Schema({
    domain: {
        type: String,
        required: [true, 'Domain name is required!'],
        unique: [true, 'Domain by that name is already in use!'],
    },
    verification_key: {
        type: String,
        unique: [true, 'Verification key is already in use!'],
        required: [true, 'Verification Key for the domain is required!'],
    },
    status: {
        type: String,
        enum: ['PENDING', 'VERIFIED'],
        default: 'PENDING',
    },
});

module.exports = mongoose.model('Domain', domainSchema);

const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

const currencySchema = mongoose.Schema({
    short_name: {
        type: String,
        required: [true,'Currency abbreviation is required!'],
        unique: [true,'Currency by that abbreviation is already in the collection!']
    },
    full_name: {
        type: String,
        required: [true,'Full Currency name is required!'],
        unique: [true,'Currency by that name is already in the collection!']
    }
});

module.exports = mongoose.model('Currency', currencySchema);

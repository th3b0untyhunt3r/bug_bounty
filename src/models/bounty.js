const mongoose = require('mongoose');

const bountySchema = mongoose.Schema({
    reports: [
        {
            type: mongoose.Types.ObjectId,
            ref: 'Report',
        },
    ],
    name: {
        type: String,
        required: [true, 'Bounty name required!'],
        unique: [true, 'Bounty name must be unique!'],
        max: 70,
    },
    company: {
        type: mongoose.Types.ObjectId,
        ref: 'Company',
    },
    created_at: {
        type: Date,
        default: Date.now,
    },
    domains: [
        {
            type: mongoose.Types.ObjectId,
            ref: 'Domain',
        },
    ],
    policy: {
        type: String,
        required: [true, 'Bounty policy is required!'],
    },
    low_price: {
        type: Number,
    },
    medium_price: {
        type: Number,
    },
    high_price: {
        type: Number,
    },
    currency: {
        type: mongoose.Types.ObjectId,
        ref: 'Currency',
    },
    is_private: {
        type: Boolean,
        default: false,
    },
});

module.exports = mongoose.model('Bounty', bountySchema);

const mongoose = require('mongoose');

const reportSchema = mongoose.Schema({
  name: {
    type: String,
    required: [true, 'Report name is required field!'],
  },
  content: {
    type: String,
    required: [true, 'Report content is required field!'],
  },
  status: {
    type: String,
    enum: ['PENDING', 'ACCEPTED', 'REJECTED'],
    default: 'PENDING',
  },
  hacker: {
    type: mongoose.Types.ObjectId,
    ref: 'Hacker',
  },
  bounty: {
    type: mongoose.Types.ObjectId,
    ref: 'Bounty',
  },
});

module.exports = mongoose.model('Report', reportSchema);

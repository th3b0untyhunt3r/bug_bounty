const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const crypto = require('crypto');

const responsibleSchema = mongoose.Schema({
    first_name: {
        type: String,
        trim: true,
        required: [true, 'First name is required!'],
    },
    last_name: {
        type: String,
        trim: true,
        required: [true, 'Last name is required!'],
    },
    email: {
        type: String,
        validate: {
            validator: function (email) {
                return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
                    email
                );
            },
            message: props => `${props.value} is not a valid e-mail address!`,
            // bu regexde email a@salam.b sheklinde ola bilmez noqteden sonra en azi 2 herf gelmelidir cunki 1 herfli domain adi yoxdu
        },
        required: [true, 'E-mail is required!'],
        trim: true,
        unique: [true, 'E-mail address is already registered!'],
    },
    password: {
        type: String,
        required: [true, 'Password required!'],
        validate: {
            validator: function (password) {
                return /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,50})/.test(password);
            },
            message: props => `Password "${props.value}" is not strong enough!`,
            // bu regexde passwordda en azi 1 balaca 1 boyuk 1 reqem 1 special char olmalidir, minimum 8 maksimum 50
        },
    },
    company: {
        type: mongoose.Types.ObjectId,
        required: [true, 'Company is required for the current responsible!'],
        ref: 'Company',
    },
    position: {
        type: String,
        required: [true, 'Position is required!'],
    },
    reset_token: {
        type: String,
    },
    reset_expires: {
        type: Date,
    },
});

responsibleSchema.pre('save', function (next) {
    if (this.isModified('password')) {
        const salt = bcrypt.genSaltSync(10);
        const hashedPassword = bcrypt.hashSync(this.password, salt);
        this.password = hashedPassword;
    }
    next();
});

responsibleSchema.methods.isMatchedPassword = function (verifyPassword) {
    return bcrypt.compareSync(verifyPassword, this.password);
};

responsibleSchema.methods.getSignedJWTToken = function () {
    const payload = {
        resId: this._id,
        comId: this.company._id,
        status: this.company.status,
    };
    const options = {
        expiresIn: process.env.JWT_EXP_DATE,
    };

    return jwt.sign(payload, process.env.JWT_SECRET_KEY, options);
};

responsibleSchema.methods.setResetToken = function () {
    this.reset_token = crypto.randomBytes(20).toString('hex');
    this.reset_expires = Date.now() + 1800000;
};

module.exports = mongoose.model('Responsible', responsibleSchema);

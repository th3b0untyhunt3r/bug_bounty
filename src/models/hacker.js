const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const crypto = require('crypto');
const { PATH } = require('../constants/paths');

const hackerSchema = mongoose.Schema({
  reports: [
    {
      type: mongoose.Types.ObjectId,
      ref: 'Report',
    },
  ],
  first_name: {
    type: String,
    required: [true, 'First name is required!'],
    trim: true,
  },
  last_name: {
    type: String,
    required: [true, 'Last name is required!'],
    trim: true,
  },
  username: {
    type: String,
    required: [true, 'Username is required!'],
    unique: [true, 'Username is already taken!'],
    lowercase: true,
    validate: {
      validator: function (username) {
        return /^(?=.{6,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$/.test(username);
      },
      message: props => `${props.value} is not a valid username!`,
      // bu regexde usernamede evvelde _ ve ya . gele bilmez, ortada yanashi iki dene __ ve ya .. olmaz
    },
  },
  // regex for password
  password: {
    type: String,
    required: [true, 'Password is required!'],
    validate: {
      validator: function (password) {
        return /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,50})/.test(password);
      },
      message: props => `Password "${props.value}" is not strong enough!`,
      // bu regexde passwordda en azi 1 balaca 1 boyuk 1 reqem 1 special char olmalidir, minimum 8 maksimum 50
    },
  },
  avatar: {
    type: String,
    default: 'default-avatar.png',
  },
  home_address: {
    type: String,
    required: [true, 'Home address is required!'],
    default: '',
    max: 100,
  },
  email: {
    type: String,
    validate: {
      validator: function (email) {
        return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
          email
        );
      },
      message: props => `${props.value} is not a valid e-mail address!`,
    },
    required: [true, 'E-mail address is required!'],
    trim: true,
    unique: [true, 'E-mail address is already registered!'],
  },
  bio: {
    type: String,
    trim: true,
    default: '',
  },
  voen: {
    type: String,
    unique: [true, 'VOEN number is already registered!'],
    required: [true, 'VOEN is required!'],
    validate: {
      validator: function (voen) {
        return /^\d{10}$/.test(voen);
      },
      message: props => `${props.value} is not a valid VOEN!`,
      // bu regexde sadece 10 digit ola biler hansiki VOEN formasidir
    },
  },
  linkedin: {
    type: String,
    // unique: [true, 'Account in the given URL is already in use!'],
    validate: {
      validator: function (url) {
        if (/^$/.test(url)) return true;
        return /^((https?:\/\/)?((www|\w\w)\.)?linkedin\.com\/)((([\w]{2,3})?)|([^\/]+\/(([\w|\d-&#?=])+\/?){1,}))$/.test(url);
      },
      message: props => `${props.value} is not a valid Linkedin URL!`,
      // LinkedIn ucun xususi URL regexi
    },
    sparse: true,
    default: '',
  },
  facebook: {
    type: String,
    // unique: [true, 'Account in the given URL is already in use!'],
    validate: {
      validator: function (url) {
        if (/^$/.test(url)) return true;
        return /^(?:(?:http|https):\/\/)?(?:www.)?facebook.com\/(?:(?:\w)*#!\/)?(?:pages\/)?(?:[?\w\-]*\/)?(?:profile.php\?id=(?=\d.*))?([\w\-]*)?$/.test(
          url
        );
      },
      message: props => `${props.value} is not a valid Facebook profile / page!`,
      // Facebook ucun xususi URL regexi
    },
    sparse: true,
    default: '',
  },
  instagram: {
    type: String,
    // unique: [true, 'Account in the given URL is already in use!'],
    validate: {
      validator: function (url) {
        if (/^$/.test(url)) return true;
        return /^\s*(http\:\/\/)?instagram\.com\/[a-z\d-_]{1,255}\s*$/i.test(url);
      },
      message: props => `${props.value} is not a valid Instagram URL!`,
      // Instagram ucun xususi URL regexi
    },
    sparse: true,
    default: '',
  },
  status: {
    type: String,
    enum: ['PENDING', 'HACKER', 'PRO'],
    default: 'PENDING',
  },
  cv: {
    type: String,
    unique: [true, 'CV by that path is already in use!'],
    sparse: true,
  },
  reputation: {
    type: Number,
    default: 0,
  },
  is_private: {
    type: Boolean,
    default: false,
  },
  reset_token: {
    type: String,
  },
  reset_expires: {
    type: Date,
  },
});

hackerSchema.pre('save', function (next) {
  if (this.isModified('password')) {
    const salt = bcrypt.genSaltSync(10);
    const hashedPassword = bcrypt.hashSync(this.password, salt);
    this.password = hashedPassword;
  }
  next();
});

hackerSchema.methods.isMatchedPassword = function (verifyPassword) {
  return bcrypt.compareSync(verifyPassword, this.password);
};

hackerSchema.methods.getSignedJWTToken = function () {
  const payload = {
    hackerId: this._id,
    username: this.username,
    status: this.status,
  };
  const options = {
    expiresIn: process.env.JWT_EXP_DATE,
  };
  return jwt.sign(payload, process.env.JWT_SECRET_KEY, options);
};

hackerSchema.methods.setResetToken = function () {
  this.reset_token = crypto.randomBytes(20).toString('hex');
  this.reset_expires = Date.now() + 1800000;
};

module.exports = mongoose.model('Hacker', hackerSchema);

const path = require('path');
const multer = require('multer');

exports.uploadImageHandler = (imagePath, size = 1024 * 1024 * 5) => {

        const storage = multer.diskStorage({
            destination: function(req, file, cb){
                cb(null, imagePath);
            },
            filename: function(req, file, cb){
                cb(null, Date.now().toString() + path.extname(file.originalname));
            }
        });
        
        const fileFilter = (req, file, cb) => {
            if ( file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
                cb(null, true);
            } else {
                cb(new ErrorResponse('File type is not supported!', 403), false);
            }
        };
        
        const upload = multer({ 
            storage: storage,
            limits: { fileSize: size },
            fileFilter: fileFilter
        });
        
        return upload;
}
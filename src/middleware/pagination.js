// not completed
const ErrorResponse = require('../utils/errorResponse');

const fields = {'bounty': ['name', 'company'], 'hacker': ['username'], 'company': ['name']};

exports.paginate = (model, find_obj, select_string, sort_obj=null, populate_obj=null) => {
    return async (req, res, next) => {
        const limit = parseInt(req.query.limit);
        const page = parseInt(req.query.page);
        const model_name = model[1];
        
        let find;
        Array.isArray(find_obj) ? find = find_obj[0][req.user.status] : find_obj;

        if (req.query.search) {
            const regex = new RegExp(escapeRegex(req.query.search), 'gi');
            const conditions = fields[model_name].map(field => {
                return {[field]: regex}
            });
            find = {...find, "$or": conditions};
        }

        results = {};
        results.totalNumber = await model[0].countDocuments(find);

        const startIndex = (page - 1) * limit;
        const endIndex = page * limit;

        if (startIndex > 0) {
            results.previous = {
                limit: limit,
                page: page - 1,
            };
        }

        if (endIndex < results.totalNumber) {
            results.next = {
                limit: limit,
                page: page + 1,
            };
        }
        
        try {
            results.results = await model[0].find(find).select(select_string)
              .skip(startIndex).limit(limit).sort(sort_obj)
              .populate(populate_obj).lean();
            res.results = results;
            next();
        } catch (err) {
            return new ErrorResponse('Error!', 500);
        }
    };
};

function escapeRegex(text) {
    return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
};
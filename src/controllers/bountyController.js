const express = require('express');
const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require('../middleware/async');
const Responsible = require('../models/responsible');
const Company = require('../models/company');
const Bounty = require('../models/bounty');
const Domain = require('../models/domain');
const { deleteDomain } = require('./domainController');

//@desc   Create Bounty
//@route  POST /profile/company/bounty/create
//@access PRIVATE : 'VERIFIED'
// TODO test currency
exports.createBounty = asyncHandler(async (req, res, next) => {
    let { name, domains, policy, low_price, medium_price, high_price, currency, is_private } = req.body;
    domains = Array.from(new Set(domains));
    
    const company = await Company.findById(req.user.comId)
        .select('name domains bounties')
        .populate({ path: 'domains', match: { status: 'VERIFIED' }, select: '_id' });

    // check every element in domains array exist in company.domains array
    const unverifiedDomains = domains.filter(
        domain => company.domains.find(verifiedDomain => verifiedDomain._id.equals(domain)) === undefined
    );

    if (unverifiedDomains.length !== 0) return next(new ErrorResponse('You have not verified some domains', 403));
    
    const bounty = new Bounty({
        name,
        domains,
        policy,
        company: req.user.comId,
        low_price,
        medium_price,
        high_price,
        currency,
        is_private,
    });

    await bounty.save();

    company.bounties.push(bounty);
    await company.save();

    return res.status(201).json({
        success: true,
        message: `Bounty created and registered to the ${company.name}'s account successfully!`,
        company,
    });
});

//@desc   Get Bounty with Id
//@route  GET /profile/company/bounty/:id
//@access PRIVATE : 'VERIFIED'
exports.getBounty = asyncHandler(async (req, res, next) => {
    const companyId = req.user.comId;
    const bountyId = req.params.id;

    const bounties = await Company.findById(companyId)
        .select('bounties')
        .populate({ path: 'bounties', select: "name status", match: { _id: bountyId } });

    if (!bounties.length === 0) {
        return next(new ErrorResponse('Bounty does not exist!', 404));
    }

    return res.status(200).json({
        success: true,
        message: `Bounty is successfully fetched!`,
        bounty: bounties.bounties[0],
    });
});

//@desc   Update Bounty with Id
//@route  PUT /profile/company/bounty/:id
//@access PRIVATE : 'VERIFIED'
//TODO test currency
exports.updateBounty = asyncHandler(async (req, res, next) => {
    const update = ({ name, domains, policy, low_price, medium_price, high_price, currency, is_private } = req.body);
    update.domains = Array.from(new Set(domains));

    const companyId = req.user.comId;
    const bountyId = req.params.id;

    const company = await Company.findById(companyId)
        .select('bounties domains')
        .populate({ path: 'bounties', match: { _id: bountyId } })
        .populate({ path: 'domains', select: "domain", match: { status: 'VERIFIED' } });

    // check every element in domains array exist in company.domains array
    const unverifiedDomains = domains.filter(
        domain => company.domains.find(verifiedDomain => verifiedDomain._id.equals(domain)) === undefined
    );

    if (unverifiedDomains.length !== 0) return next(new ErrorResponse('You have not verified some domains', 403));
    if (company.bounties.length === 0) return next(new ErrorResponse('Bounty does not exist!', 404));

    const bounty = await Bounty.findOneAndUpdate({ _id: req.params.id }, update, { new: true, runValidators: true });

    if (!bounty) {
        return next(new ErrorResponse('Bounty does not exist!', 404));
    }

    return res.status(200).json({
        success: true,
        message: `Bounty Updated Successfully!`,
        company,
    });
});

//@desc   Delete Bounty with Id
//@route  DELETE /profile/company/bounty/:id
//@access PRIVATE : 'VERIFIED'
exports.deleteBounty = asyncHandler(async (req, res, next) => {
    const companyId = req.user.comId;
    const bountyId = req.params.id;

    const company = await Company.findById(companyId)
        .select('bounties')
        .populate({ path: 'bounties', match: { _id: bountyId } });
    
    if (company.bounties.length === 0) return next(new ErrorResponse('Bounty does not exist!', 404));

    company.bounties.remove(bountyId);
    await company.save();

    await Bounty.findByIdAndDelete(bountyId);

    return res.status(200).json({
        success: true,
        message: `Bounty Deleted Successfully!`,
    });
});

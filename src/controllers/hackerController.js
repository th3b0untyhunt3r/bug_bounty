const express = require('express');
const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require('../middleware/async');
const Hacker = require('../models/hacker');
const Bounty = require('../models/bounty');
const Domain = require('../models/domain');
const Company = require('../models/company');
const Report = require('../models/report');

//@desc   Hacker leaderboard
//@route  GET /hacker/leaderboard
//@access PUBLIC
exports.getLeaderboard = asyncHandler(async (req, res, next) => {
  let rep;
  res.results.results = await Promise.all(
    res.results.results.map(async hacker => {
      rep = await Hacker.find({ reputation: { $gt: hacker.reputation } })
        .distinct('reputation')
        .exec();
      hacker.rank = rep.length + 1;
      if (hacker.is_private) {
        hacker.avatar = 'private-avatar.jpg';
        hacker.first_name = 'Private';
        hacker.last_name = 'Account';
      }
      return hacker;
    })
  );
  return res.status(200).json({ success: true, results });
});

//@desc   Get bounties for hackers
//@route  GET /hacker/bounty/:id
//@access PRIVATE : 'HACKER','PRO'
exports.getBountyForHackers = asyncHandler(async (req, res, next) => {
  const bountyId = req.params.id;

  const bounty = await Bounty.findOne({ _id: bountyId })
    .select('domains is_private _id name policy company low_price medium_price high_price currency created_at')
    .populate({
      path: 'company',
      select: 'name',
    })
    .populate({
      path: 'domains',
      select: ' -verification_key',
    })
    .populate({
      path: 'currency',
    });

  if (!bounty) {
    return next(new ErrorResponse('Bounty does not exist!', 404));
  }

  if (bounty.is_private === true && req.user.status != 'PRO') {
    return next(new ErrorResponse('This is a private bounty and only PRO verified users can have access!', 403));
  }

  return res.status(200).json({ success: true, bounty });
});

//@desc  Submit new report
//@route  Post /hacker/report/create
//@access PRIVATE : 'HACKER','PRO'
exports.submitReport = asyncHandler(async (req, res, next) => {
  const hackerId = req.user.hackerId;

  const { bountyId, name, content } = req.body;

  const reportModel = new Report({
    name,
    content,
    bounty: bountyId,
    hacker: hackerId,
  });

  await reportModel.save();

  return res.status(201).json({ success: true, message: 'Your report successfully submitted!' });
});

//@desc  Get all submitted reports
//@route  GET /hacker/reports
//@access PRIVATE : 'HACKER','PRO'
exports.getHackerReports = asyncHandler(async (req, res, next) => {
  const hackerId = req.user.hackerId;

  const reports = await Report.find({ hacker: hackerId }).select('name status').populate({ path: 'bounty', select: 'name' });

  if (!reports) {
    return next(new ErrorResponse('Something went wrong.', 404));
  }

  return res.status(200).json({
    success: true,
    reports,
  });
});

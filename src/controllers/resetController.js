const express = require('express');
const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require('../middleware/async');
const Hacker = require('../models/hacker');
const Responsible = require('../models/responsible');
const { sendMail } = require('../utils/mailHandler');

//@desc   Forget password for hackers
//@route  POST /auth/hacker/forget
//@access PUBLIC
exports.forgetPasswordHacker = asyncHandler(async (req, res, next) => {
    const { email } = req.body;

    const hacker = await Hacker.findOne({ email }).select('username email reset_token reset_expires');

    if (!hacker) {
        return next(new ErrorResponse('Hacker by that email does not exist!', 404));
    }

    hacker.setResetToken();

    await hacker.save();

    sendMail({
        mailTo: hacker.email,
        mailType: 'HACKER_PASSWORD_RESET',
        options: { username: hacker.username, token: hacker.reset_token },
    });

    return res.status(200).json({
        success: true,
        message: 'Reset token has been sent to your email address!',
    });
});

// @desc   Reset password for hackers
// @route  POST /auth/hacker/reset/:token
// @access PUBLIC
exports.resetPasswordHacker = asyncHandler(async (req, res, next) => {
    const { password, passwordConfirmation } = req.body;

    const reset_token = req.params.token;

    if (password !== passwordConfirmation) {
        return next(new ErrorResponse('Passwords do not match!', 400));
    }

    const hacker = await Hacker.findOne({ reset_token, reset_expires: { $gt: Date.now() } }).select('reset_token reset_expires');

    if (!hacker) {
        return next(new ErrorResponse('Token is invalid or has expired!', 401));
    }

    hacker.password = password;
    hacker.reset_token = null;
    hacker.reset_expires = null;

    // const { err, hacker }

    await hacker.save();

    return res.status(200).json({
        success: true,
        message: 'Your password has been changed!',
    });
});

//@desc   Forget password for responsibles
//@route  POST /auth/company/forget
//@access PUBLIC
exports.forgetPasswordCompany = asyncHandler(async (req, res, next) => {
    const { email } = req.body;

    const responsible = await Responsible.findOne({ email }).select('email first_name reset_token reset_expires');

    if (!responsible) {
        return next(new ErrorResponse('Responsible by that email does not exist!', 404));
    }

    responsible.setResetToken();

    await responsible.save();

    sendMail({
        mailTo: responsible.email,
        mailType: 'RESPONSIBLE_PASSWORD_RESET',
        options: { firstName: responsible.first_name, token: responsible.reset_token },
    });

    return res.status(200).json({
        success: true,
        message: 'Reset token has been sent to your email address!',
    });
});

// @desc   Reset password for responsibles
// @route  POST /auth/company/reset/:token
// @access PUBLIC
exports.resetPasswordCompany = asyncHandler(async (req, res, next) => {
    const { password, passwordConfirmation } = req.body;

    const reset_token = req.params.token;

    if (password !== passwordConfirmation) {
        return next(new ErrorResponse('Passwords do not match!', 400));
    }

    const responsible = await Responsible.findOne({ reset_token, reset_expires: { $gt: Date.now() } }).select('reset_token reset_expires');

    if (!responsible) {
        return next(new ErrorResponse('Token is invalid or has expired!', 403));
    }

    responsible.password = password;
    responsible.reset_token = null;
    responsible.reset_expires = null;

    await responsible.save();

    return res.status(200).json({
        success: true,
        message: 'Your password has been changed!',
    });
});

const path = require('path');

exports.PATH = Object.freeze({
    AVATAR: path.resolve('uploads', 'avatars'),
    LOGO:   path.resolve('uploads', 'logos')
});